﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinColliderScript : MonoBehaviour
{
    private Vector2 initialPosition;
    private coinCollectorScript coinCollectorScript;
    // Start is called before the first frame update
    void Start()
    {
        initialPosition = transform.position;
        coinCollectorScript = GameObject.FindGameObjectWithTag("Player").GetComponent<coinCollectorScript>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("turret")|| collision.CompareTag("roadColliders"))
        {
            CoinColliderManager();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        CoinColliderManager();

    }
    private void CoinColliderManager()
    {
        transform.position = initialPosition;
        coinCollectorScript.coinsGathered--;
    }
}
